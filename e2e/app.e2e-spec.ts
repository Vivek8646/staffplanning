import { StaffplanningPage } from './app.po';

describe('staffplanning App', () => {
  let page: StaffplanningPage;

  beforeEach(() => {
    page = new StaffplanningPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
